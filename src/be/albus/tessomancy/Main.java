package be.albus.tessomancy;

import be.albus.tessomancy.listeners.TessomancyListener;
import be.albus.tessomancy.listeners.TessomancySuccessListener;
import org.bukkit.Bukkit;
import org.bukkit.plugin.java.JavaPlugin;

public class Main extends JavaPlugin {


    @Override
    public void onDisable() {
        super.onDisable();
    }

    @Override
    public void onEnable() {
        super.onEnable();
        Bukkit.getPluginManager().registerEvents(new TessomancyListener(this), this);
        Bukkit.getPluginManager().registerEvents(new TessomancySuccessListener(this), this);
        this.saveDefaultConfig();
    }

}
