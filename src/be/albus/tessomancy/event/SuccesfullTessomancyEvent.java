package be.albus.tessomancy.event;

import be.albus.tessomancy.model.Prophecy;
import org.bukkit.event.Cancellable;
import org.bukkit.event.Event;
import org.bukkit.event.HandlerList;

public class SuccesfullTessomancyEvent extends Event implements Cancellable {

    private static final HandlerList handlers = new HandlerList();
    private Prophecy prophecy;

    public SuccesfullTessomancyEvent(Prophecy prophecy) {
        this.prophecy = prophecy;
    }

    @Override
    public boolean isCancelled() {
        return false;
    }

    @Override
    public void setCancelled(boolean b) {

    }

    @Override
    public HandlerList getHandlers() {
        return handlers;
    }

    public static HandlerList getHandlerList() {
        return handlers;
    }

    public Prophecy getProphecy() {
        return prophecy;
    }
}
