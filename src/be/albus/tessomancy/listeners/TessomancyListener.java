package be.albus.tessomancy.listeners;

import be.albus.tessomancy.event.SuccesfullTessomancyEvent;
import be.albus.tessomancy.model.Prophecy;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.conversations.*;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.inventory.InventoryCloseEvent;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.event.player.PlayerQuitEvent;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.plugin.java.JavaPlugin;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;

import java.io.File;
import java.time.LocalDateTime;
import java.util.*;

public class TessomancyListener implements Listener, ConversationAbandonedListener {

    private final JavaPlugin plugin;

    private List<UUID> tessomancyPlayers;
    private Map<UUID, Integer> amountOfSteps;
    private Map<UUID, String> answerPerPlayer;
    private List<UUID> correctlyClosedInventory;
    private ConversationFactory conversationFactory;


    public TessomancyListener(JavaPlugin plugin) {
        this.plugin = plugin;
        amountOfSteps = new HashMap<>();
        tessomancyPlayers = new ArrayList<>();
        answerPerPlayer = new HashMap<>();
        correctlyClosedInventory = new ArrayList<>();
        this.conversationFactory = new ConversationFactory(plugin)
                .withModality(true)
                .withPrefix(new TessomancyConversationPrefix())
                .withFirstPrompt(new TessomancyStartPrompt())
                .withEscapeSequence("/quit")
                .withTimeout(plugin.getConfig().getInt("tessomancy-chatTimeout"))
                .withLocalEcho(false)
                .thatExcludesNonPlayersWithMessage("Go away evil console!")
                .addConversationAbandonedListener(this);
    }

    @EventHandler
    public void onUseEvent(PlayerInteractEvent e) {
        Player player = e.getPlayer();
        String itemToUseForTessomancy = plugin.getConfig().getString("tessomancy-itemToUse");
        if (player.getInventory().getItemInMainHand().getType() == Material.valueOf(itemToUseForTessomancy) && !tessomancyPlayers.contains(player.getUniqueId())) {
            tessomancyPlayers.add(player.getUniqueId());
            amountOfSteps.put(player.getUniqueId(), 1);
            player.sendMessage(ChatColor.GREEN + "You take a sip from the teacup and see a pattern appear.. ");
            tessomancyEvent(player);
        }
    }

    private void tessomancyEvent(Player player) {
        if (plugin.getConfig().getInt("tessomancy-completionSteps") >= amountOfSteps.get(player.getUniqueId())) {
            List<String> patterns = plugin.getConfig().getStringList("tessomancy-patterns");
            Random random = new Random();
            int chosen = random.nextInt(patterns.size());
            String chosenPattern = patterns.get(chosen);
            answerPerPlayer.put(player.getUniqueId(), getAnswer(chosenPattern));
            List<String> patternsFromConfig = getPattern(chosenPattern);
            Inventory inventory = Bukkit.createInventory(null, 54, "Tessomancy");
            ItemStack itemstack = new ItemStack(Material.LEAVES, 1, (byte) 1);
            ItemStack[] itemStacks = new ItemStack[54];
            for (int i = 0; i < 54; i++) {
                if (patternsFromConfig.get(i).equals("leaves")) {
                    itemStacks[i] = itemstack;
                } else {
                    itemStacks[i] = new ItemStack(Material.STAINED_GLASS_PANE, 1, (byte) 9);
                }
            }
            player.addPotionEffect(new PotionEffect(PotionEffectType.BLINDNESS, 1000, 3));
            inventory.setContents(itemStacks);
            player.openInventory(inventory);

            Bukkit.getScheduler().scheduleSyncDelayedTask(plugin, new Runnable() {
                public void run() {
                    if (tessomancyPlayers.contains(player.getUniqueId())) {
                        correctlyClosedInventory.add(player.getUniqueId());
                        player.closeInventory();
                        conversationFactory.buildConversation(player).begin();
                    }
                }
            }, 60L);

        }
    }

    @EventHandler
    public void onClickEvent(InventoryClickEvent e) {
        if (tessomancyPlayers.contains(e.getWhoClicked().getUniqueId())) {
            e.setCancelled(true);
        }
    }

    @EventHandler
    public void onInventoryExitEvent(InventoryCloseEvent e) {
        Player player = (Player) e.getPlayer();
        if (tessomancyPlayers.contains(player.getUniqueId())) {
            if (!correctlyClosedInventory.contains(player.getUniqueId())) {
                player.sendMessage(ChatColor.RED + "You lost focus, try again next time.");
                removePlayerFromTessomancy(player);
            } else {
                correctlyClosedInventory.remove(player.getUniqueId());
            }
        }
    }


    @EventHandler
    public void onLogout(PlayerQuitEvent e) {
        removePlayerFromTessomancy(e.getPlayer());
    }

    private List<String> getPattern(String chosenPattern) {
        File customConfigFile = new File(plugin.getDataFolder(), chosenPattern + ".yml");
        FileConfiguration customConfig = YamlConfiguration.loadConfiguration(customConfigFile);
        List<String> completePatternList = new ArrayList<>();
        for (String key : customConfig.getConfigurationSection("pattern").getKeys(false)) {
            List<String> blocks = customConfig.getStringList("pattern." + key);
            completePatternList.addAll(blocks);
        }
        return completePatternList;
    }

    private String getAnswer(String chosenPattern) {
        File customConfigFile = new File(plugin.getDataFolder(), chosenPattern + ".yml");
        FileConfiguration customConfig = YamlConfiguration.loadConfiguration(customConfigFile);
        return customConfig.get("answer").toString();
    }


    public void conversationAbandoned(ConversationAbandonedEvent abandonedEvent) {
        if (!abandonedEvent.gracefulExit()) {
            abandonedEvent.getContext().getForWhom().sendRawMessage(ChatColor.RED + "You lost focus, try again next time.");
            Player player = (Player) (abandonedEvent.getContext().getForWhom());
            removePlayerFromTessomancy(player);
        }
    }


    private class TessomancyStartPrompt extends StringPrompt {
        public String getPromptText(ConversationContext context) {
            return "What did you see?";
        }

        @Override
        public Prompt acceptInput(ConversationContext conversationContext, String s) {
            Player player = (Player) (conversationContext.getForWhom());
            if (s.equalsIgnoreCase(answerPerPlayer.get(player.getUniqueId()))) {
                return new TessomancySuccesMessagePrompt();
            } else {
                return new TessomancyFailureMessagePrompt();
            }
        }
    }

    private class TessomancySuccesMessagePrompt extends MessagePrompt {

        @Override
        public String getPromptText(ConversationContext conversationContext) {
            return "Well done you guessed the word!";
        }

        @Override
        protected Prompt getNextPrompt(ConversationContext conversationContext) {
            Player player = (Player) conversationContext.getForWhom();
            int step = amountOfSteps.get(player.getUniqueId());
            amountOfSteps.replace(player.getUniqueId(), ++step);
            tessomancyEvent(player);
            if (step > plugin.getConfig().getInt("tessomancy-completionSteps")) {
                return new TessomancyEndMessagePrompt();
            }
            return Prompt.END_OF_CONVERSATION;
        }
    }

    private class TessomancyFailureMessagePrompt extends MessagePrompt {

        @Override
        public String getPromptText(ConversationContext conversationContext) {
            return ChatColor.RED + "Wrong word, please try again.";
        }

        @Override
        protected Prompt getNextPrompt(ConversationContext conversationContext) {
            Player player = (Player) conversationContext.getForWhom();
            tessomancyEvent(player);
            return Prompt.END_OF_CONVERSATION;
        }
    }

    private class TessomancyEndMessagePrompt extends MessagePrompt {
        private Prophecy prophecy;

        @Override
        public String getPromptText(ConversationContext conversationContext) {
            return "You have completed the prophecy!";
        }

        @Override
        protected Prompt getNextPrompt(ConversationContext conversationContext) {
            Player player = (Player) conversationContext.getForWhom();
            executeEndOfTessomancyActions(player);
            return Prompt.END_OF_CONVERSATION;
        }

        private void executeEndOfTessomancyActions(Player player) {
            executeConfigurableCommands();
            Random random = new Random();
            createProphecy(player, random);
            Bukkit.getPluginManager().callEvent(new SuccesfullTessomancyEvent(prophecy));
            removePlayerFromTessomancy(player);
        }

        private void createProphecy(Player player, Random random) {
            List<PotionEffectType> potionEffectTypes = new ArrayList<>(Arrays.asList(PotionEffectType.values()));
            int randomPotionEffect = random.nextInt(potionEffectTypes.size());
            int randomDuration = random.nextInt(5400) + 600;
            int randomAmlifier = random.nextInt(4) + 1;
            prophecy = new Prophecy(player.getUniqueId(), new PotionEffect(potionEffectTypes.get(randomPotionEffect), randomDuration, randomAmlifier), LocalDateTime.now());
        }
    }

    private void executeConfigurableCommands() {
        if (plugin.getConfig().get("tessomancy-commands") != null) {
            List<String> commands = plugin.getConfig().getStringList("tessomancy-commands");
            commands.forEach(command -> Bukkit.getServer().dispatchCommand(Bukkit.getConsoleSender(), command));
        }
    }


    private class TessomancyConversationPrefix implements ConversationPrefix {

        public String getPrefix(ConversationContext context) {
            return ChatColor.GREEN + "";
        }
    }

    private void removePlayerFromTessomancy(Player player) {
        tessomancyPlayers.remove(player.getUniqueId());
        amountOfSteps.remove(player.getUniqueId());
        correctlyClosedInventory.remove(player.getUniqueId());
        answerPerPlayer.remove(player.getUniqueId());
        player.removePotionEffect(PotionEffectType.BLINDNESS);
    }


}
