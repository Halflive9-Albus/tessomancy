package be.albus.tessomancy.listeners;

import be.albus.tessomancy.event.SuccesfullTessomancyEvent;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.plugin.java.JavaPlugin;

public class TessomancySuccessListener implements Listener {

    private JavaPlugin plugin;

    public TessomancySuccessListener(JavaPlugin plugin) {
        this.plugin = plugin;
    }

    @EventHandler
    public void onTessoMancySuccesEvent(SuccesfullTessomancyEvent e) {
        Bukkit.getLogger().info(e.getProphecy().toString());
        Player player = Bukkit.getPlayer(e.getProphecy().getUuid());
        player.addPotionEffect(e.getProphecy().getPotionEffect());
    }

}
