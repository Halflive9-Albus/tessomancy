package be.albus.tessomancy.model;

import org.bukkit.potion.PotionEffect;

import java.time.LocalDateTime;
import java.util.UUID;

public class Prophecy {

    private UUID uuid;
    private PotionEffect potionEffect;
    private LocalDateTime whenToApply;

    public Prophecy(UUID uuid, PotionEffect potionEffect, LocalDateTime whenToApply) {
        this.uuid = uuid;
        this.potionEffect = potionEffect;
        this.whenToApply = whenToApply;
    }

    public UUID getUuid() {
        return uuid;
    }

    public void setUuid(UUID uuid) {
        this.uuid = uuid;
    }

    public PotionEffect getPotionEffect() {
        return potionEffect;
    }

    public void setPotionEffect(PotionEffect potionEffect) {
        this.potionEffect = potionEffect;
    }

    public LocalDateTime getWhenToApply() {
        return whenToApply;
    }

    public void setWhenToApply(LocalDateTime whenToApply) {
        this.whenToApply = whenToApply;
    }

    @Override
    public String toString() {
        return "Prophecy{" +
                "uuid=" + uuid +
                ", potionEffect=" + potionEffect +
                ", whenToApply=" + whenToApply +
                '}';
    }
}


